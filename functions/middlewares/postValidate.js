const { validationResult } = require('express-validator')

function postValidate(request, response, next) {
  const validateErrors = validationResult(request)
  if (validateErrors.isEmpty()) {
    return next()
  }

  const errors = new Array()
  validateErrors.array().map((error) => {
    errors.push({
      [error.param]: error.msg
    })
    return false
  })

  return response.status(400).json({
    errors
  })
}

module.exports = postValidate
