const { admin } = require('../utils')

async function protectedRoute(request, response, next) {
  try {
    const token = request.headers.authorization.split('Bearer ')[1]
    const tokenVerified = await admin.auth().verifyIdToken(token)
    request.user = tokenVerified.uid
    return next()
  } catch (error) {
    const { code } = error
    const message =
      error === 'auth/id-token-expired'
        ? 'The token has expired'
        : 'Unhautorized'
    return response.status(401).json({
      code,
      message
    })
  }
}

module.exports = {
  protectedRoute
}
