const { db } = require('../../utils')

async function createCollection(request, response) {
  const name = request.body.name
  const userId = request.user
  const nowDate = new Date().toISOString()
  try {
    const collection = {
      userId,
      name,
      createdAt: nowDate,
      updatedAt: nowDate
    }
    const collectionCreated = await db.collection('collections').add(collection)
    collection.id = collectionCreated.id
    await collectionCreated.update({
      id: collection.id
    })
    return response.status(201).json({
      message: 'collection created successfully',
      collection
    })
  } catch (error) {
    const { code, message } = error
    return response.status(403).json({
      code,
      message
    })
  }
}

async function editCollection(request, response) {
  const { collectionId, name } = request.body
  const userId = request.user
  try {
    const error = { code: undefined, message: undefined }
    const collectionDocument = db.doc(`/collections/${collectionId}`)
    const collection = await collectionDocument.get()
    const collectionUserId = collection.data().userId
    if (!collection.exists) {
      error.code = 'collection-not-found'
      error.message = 'The collection has not been found'
      throw error
    }
    if (collectionUserId !== userId) {
      error.code = 'user/unauthorized'
      error.message = 'Unauthorized'
      error.status = 401
      throw error
    }
    await collectionDocument.update({
      name,
      updatedAt: new Date().toISOString()
    })
    return response.status(200).json({
      message: `Collection ${collectionId} name updated successfully to ${name}`
    })
  } catch (error) {
    const { code, message } = error
    return response.status(error.status || 400).json({
      code,
      message
    })
  }
}

async function getCollectionsByUser(request, response) {
  const userId = request.user
  try {
    const collectionsDocument = await db
      .collection('collections')
      .where('userId', '==', userId)
      .orderBy('createdAt', 'asc')
      .get()
    const collections = collectionsDocument.docs.map((doc) => {
      return doc.data()
    })
    return response.status(200).json({
      collections
    })
  } catch (error) {
    const { code, message } = error
    return response.status(400).json({
      code,
      message
    })
  }
}

async function deleteCollection(request, response) {
  const id = request.body.collectionId
  const userId = request.user
  try {
    const collectionToDelete = await db.doc(`/collections/${id}`)
    const collectionDocument = await collectionToDelete.get()
    const collectionUserId = collectionDocument.data().userId
    const error = { code: undefined, message: undefined }
    if (!collectionDocument.exists) {
      error.code = 'collection-not-found'
      error.message = 'The collection has not been found'
      throw error
    }
    // Check that the collection owner is the same user logged in
    if (collectionUserId !== userId) {
      error.code = 'collection-not-found'
      error.message = 'The collection has not been found'
      error.status = 401
      throw error
    }
    // delete all tasks in the collection
    const tasksByCollection = await db
      .collection('tasks')
      .where('collectionId', '==', id)
      .get()
    // Delete all related tasks
    tasksByCollection.forEach((doc) => {
      doc.ref.delete()
    })
    // finally delete the collection
    await collectionToDelete.delete()
    // return json
    return response.status(200).json({
      message: `Collection ${id} successfully deleted`
    })
  } catch (error) {
    const { code, message } = error
    return response.status(error.status || 400).json({
      code,
      message
    })
  }
}

async function getSingleCollection(request, response) {
  const collectionId = request.params.collectionId
  const userId = request.user
  try {
    const error = { code: undefined, message: undefined }
    const collectionDocument = await db
      .doc(`/collections/${collectionId}`)
      .get()
    if (!collectionDocument.exists) {
      error.code = 'no-collection-found'
      error.message = 'Collection has not been found'
      throw error
    } else if (collectionDocument.data().userId !== userId) {
      error.code = 'user/unauthorized'
      error.message = 'Unauthorized'
      error.status = 401
      throw error
    }
    const tasksOfThisCollection = await db
      .collection('tasks')
      .where('collectionId', '==', collectionId)
      .get()
    const collection = collectionDocument.data()
    collection.tasks = []
    tasksOfThisCollection.forEach((doc) => {
      collection.tasks.push(doc.data())
    })
    return response.status(200).json({
      collection
    })
  } catch (error) {
    const { code, message } = error
    return response.status(error.status || 400).json({
      code,
      message
    })
  }
}

module.exports = {
  getCollectionsByUser,
  createCollection,
  deleteCollection,
  editCollection,
  getSingleCollection
}
