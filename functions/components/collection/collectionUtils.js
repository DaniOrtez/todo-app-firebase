const { checkSchema } = require('express-validator')

const validateCollection = () => {
  return checkSchema({
    name: {
      notEmpty: {
        errorMessage: 'The collection name field can not be empty'
      },
      isString: {
        errorMessage: 'It can only contain strings'
      }
    }
  })
}

module.exports = {
  validateCollection
}
