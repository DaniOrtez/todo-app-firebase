const { Router } = require('express')
const { protectedRoute } = require('../../middlewares/auth')
const { validateCollection } = require('./collectionUtils')
const {
  getCollectionsByUser,
  deleteCollection,
  editCollection,
  createCollection,
  getSingleCollection
} = require('./collectionController')
const postValidate = require('../../middlewares/postValidate')
const tasks = require('../tasks')

const collection = Router()

collection.get('/', protectedRoute, getCollectionsByUser)
collection.get('/:collectionId', protectedRoute, getSingleCollection)

collection.post(
  '/',
  validateCollection(),
  postValidate,
  protectedRoute,
  createCollection
)
collection.delete('/', protectedRoute, deleteCollection)
collection.patch(
  '/',
  validateCollection(),
  postValidate,
  protectedRoute,
  editCollection
)

collection.use('/task', tasks)

module.exports = collection
