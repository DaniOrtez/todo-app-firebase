const { db } = require('../../utils')

async function createTask(request, response) {
  const { collectionId, name } = request.body
  const nowDate = new Date().toISOString()
  try {
    const collectionDoc = await db.doc(`/collections/${collectionId}`).get()
    if (!collectionDoc.exists) {
      const error = {
        code: 'collection-not-found',
        message: 'The collection has not been found'
      }
      throw error
    }
    const task = {
      collectionId,
      name,
      done: false,
      createdAt: nowDate,
      updatedAt: nowDate
    }
    const taskCreated = await db.collection('tasks').add(task)
    task.id = taskCreated.id
    await taskCreated.update({
      id: taskCreated.id
    })
    return response.status(201).json({
      message: `Task ${name} created with an id ${taskCreated.id} successfully`,
      task
    })
  } catch (error) {
    const { code, message } = error
    return response.status(400).json({
      code,
      message
    })
  }
}

async function deleteTask(request, response) {
  const taskId = request.params.taskId
  try {
    const taskDoc = db.doc(`/tasks/${taskId}`)
    const task = await taskDoc.get()
    if (!task.exists) {
      const error = {
        code: 'not-found',
        message: 'Task has not been found'
      }
      throw error
    }
    await taskDoc.delete()
    return response.status(200).json({
      message: 'Task successfully deleted'
    })
  } catch (error) {
    const { code, message } = error
    return response.status(400).json({
      code,
      message
    })
  }
}

async function editTask(request, response) {
  const taskId = request.params.taskId
  try {
    const taskDoc = db.doc(`/tasks/${taskId}`)
    const task = await taskDoc.get()
    if (!task.exists) {
      const error = {
        code: 'not-found',
        message: 'Task has not been found'
      }
      throw error
    }
    await taskDoc.update(request.body)
    return response.status(200).json({
      message: 'Tasks successfully updated'
    })
  } catch (error) {
    const { code, message } = error
    return response.status(400).json({
      code,
      message
    })
  }
}

module.exports = {
  createTask,
  deleteTask,
  editTask
}
