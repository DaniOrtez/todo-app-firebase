const { checkSchema } = require('express-validator')

const validateTask = () => {
  return checkSchema({
    name: {
      notEmpty: {
        errorMessage: 'The task name field can not be empty'
      }
    },
    done: {
      isBoolean: {
        errorMessage: 'It can only have booleans'
      },
      notEmpty: {
        errorMessage: 'The task done field can not be empty'
      }
    }
  })
}

module.exports = { validateTask }
