const { Router } = require('express')
const { protectedRoute } = require('../../middlewares/auth')
const { createTask, deleteTask, editTask } = require('./tasksController')
const { validateTask } = require('./tasksUtils')
const postValidate = require('../../middlewares/postValidate')

const tasks = Router()

tasks.post('/', validateTask(), postValidate, protectedRoute, createTask)
tasks.delete('/:taskId', protectedRoute, deleteTask)
tasks.patch('/:taskId', validateTask(), protectedRoute, editTask)

module.exports = tasks
