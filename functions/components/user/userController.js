const { firebase, db, config } = require('../../utils')

async function register(request, response) {
  const { email, password, username } = request.body
  const nowDate = new Date().toISOString()
  const profileImage = `https://firebasestorage.googleapis.com/v0/b/${config.storageBucket}/o/${config.defaultProfileImage}?alt=media`
  let userCreated
  let userId
  let token

  try {
    const userDocument = await db.doc(`/users/${username}`).get()
    if (userDocument.exists) {
      const error = {
        code: 'username-already-taken',
        message: 'This username is already taken',
        status: 403
      }
      throw error
    }

    // Create user
    userCreated = await firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)

    await userCreated.user.updateProfile({
      displayName: username,
      photoURL: profileImage
    })

    // Set userId and Token
    userId = userCreated.user.uid
    token = await userCreated.user.getIdToken()

    // Create the new user document
    await db.doc(`/users/${username}`).set({
      id: userId,
      email,
      username,
      profileImage,
      createdAt: nowDate,
      updatedAt: nowDate
    })

    return response.status(201).json({
      message: 'User created successfully',
      userId,
      token
    })
  } catch (error) {
    await userCreated.user.delete()
    const { code, message } = error
    const status = error.status || 500
    return response.status(status).json({
      code,
      message
    })
  }
}

async function login(request, response) {
  const { email, password } = request.body
  try {
    const userLogged = await firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
    const token = await userLogged.user.getIdToken()
    return response.status(200).json({
      user: `${userLogged.user.displayName} successfully logged`,
      token
    })
  } catch (error) {
    const { code, message } = error
    return response.status(403).json({
      code,
      message
    })
  }
}

async function getUser(request, response) {
  const userId = request.user
  try {
    const userDocument = await db.doc(`/users/${userId}`).get()
    const data = await userDocument.data()
    if (userDocument.id !== userId) {
      return response.status(401).json({
        message: 'Unauthorized'
      })
    }
    return response.status(200).json({
      data
    })
  } catch (error) {
    const { code, message } = error
    return response.status(400).json({
      code,
      message
    })
  }
}

module.exports = {
  register,
  login,
  getUser
}
