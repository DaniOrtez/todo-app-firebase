const { checkSchema } = require('express-validator')

const validateRegister = () => {
  // Check Schema
  return checkSchema({
    email: {
      trim: true,
      isEmail: {
        options: { allow_display_name: true },
        errorMessage: 'You must enter a valid email'
      },
      notEmpty: {
        errorMessage: 'The email field can not be empty'
      }
    },
    password: {
      trim: true,
      isLength: {
        errorMessage: 'Password must be at least 8 characters long',
        options: { min: 8 }
      },
      notEmpty: {
        errorMessage: 'The password field can not be empty'
      },
      matches: {
        options: /\d/,
        errorMessage: 'The password must have at least 1 number'
      }
    },
    confirmPassword: {
      trim: true,
      custom: {
        options: (value, { req }) => {
          if (value !== req.body.password) {
            throw new Error('Password confirmation does not match password')
          }
          return true
        }
      }
    },
    username: {
      trim: true,
      isString: true,
      isLength: {
        options: { min: 4 },
        errorMessage: 'The username must be at least 4 chars long'
      },
      notEmpty: {
        errorMessage: 'The username field can not be empty'
      }
    }
  })
}

const validateLogin = () => {
  return checkSchema({
    email: {
      trim: true,
      isEmail: {
        errorMessage: 'You must enter a valid email'
      },
      notEmpty: {
        errorMessage: 'The email field can not be empty'
      }
    },
    password: {
      trim: true,
      isLength: {
        options: { min: 8 }
      },
      notEmpty: {
        errorMessage: 'The password field can not be empty'
      }
    }
  })
}

module.exports = {
  validateRegister,
  validateLogin
}
