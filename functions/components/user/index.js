const { Router } = require('express')
const { validateRegister, validateLogin } = require('./userUtils')
const { protectedRoute } = require('../../middlewares/auth')
const { getUser, login, register } = require('./userController')
const postValidate = require('../../middlewares/postValidate')

const user = Router()

user.get('/', protectedRoute, getUser)

user.post('/register', validateRegister(), postValidate, register)

user.post('/login', validateLogin(), postValidate, login)

module.exports = user
