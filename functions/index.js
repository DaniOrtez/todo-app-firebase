const functions = require('firebase-functions')
const express = require('express')
const cors = require('cors')
const api = require('./api')
const app = express()

app.use(cors())
app.use(express.json())

app.use('/api', api)

exports.app = functions.region('europe-west1').https.onRequest(app)
