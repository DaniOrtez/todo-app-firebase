const { Router } = require('express')
const user = require('./components/user')
const collections = require('./components/collection')

const router = Router()
const version = 'v' + require('./package.json').version.split('.')[0]

router.get(`/${version}`, (_request, response) => {
  response.status(200).json({
    message: 'API WORKING'
  })
})

router.use(`/${version}/user`, user)
router.use(`/${version}/collections`, collections)

module.exports = router
