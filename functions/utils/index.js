const firebase = require('firebase')
const admin = require('firebase-admin')
const config = require('./config')

admin.initializeApp()

firebase.initializeApp({
  apiKey: config.apiKey,
  authDomain: config.authDomain,
  databaseURL: config.databaseURL,
  // projectId: Must be a string
  projectId: `${config.projectId}`,
  storageBucket: config.storageBucket,
  messagingSenderId: config.messagingSenderId,
  appId: config.appId
})

const db = firebase.firestore()

module.exports = {
  db,
  firebase,
  admin,
  config
}
