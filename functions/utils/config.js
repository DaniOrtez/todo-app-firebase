const dotenv = require('dotenv')

const { parsed, error } = dotenv.config()

if (error) {
  console.error(error)
  throw error
}

module.exports = parsed
